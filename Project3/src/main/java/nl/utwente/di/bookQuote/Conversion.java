package nl.utwente.di.bookQuote;

public class Conversion {
    static double conversion(String celsiusDegrees) {
        return Double.parseDouble(celsiusDegrees) * 1.8 + 32.0;
    }
}
