package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets a degrees number in Celsius and returns the corresponding number in Fahrenheit
 */

public class CelsiusToFahrenheit extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conversion conv;
	
    public void init() throws ServletException {
    	conv = new Conversion();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius-Fahrenheit conversion";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Degrees-Celsius: " +
                   request.getParameter("degrees") + "\n" +
                "  <P>Degrees-Fahrenheit: " +
            Double.toString(conv.conversion(request.getParameter("degrees"))) +
                "</BODY></HTML>");
  }
}
