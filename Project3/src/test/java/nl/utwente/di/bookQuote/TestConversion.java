package nl.utwente.di.bookQuote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
public class TestConversion {
    @Test
    public void testConversion() throws Exception {
        Conversion conversion = new Conversion();
        double celsiusDegrees = Conversion.conversion("7");
        Assertions.assertEquals(44.6, celsiusDegrees, 0.0, "Degrees");
    }
}
